# Region Open Data Status

Visualization of regions open data publication status in Sweden

## Description

The Swedish govenment launched a [directive](https://www.regeringen.se/om-webbplatsen/psi-direktivet/) which requires the public sector to publish data openly to the public. [Nationella Dataverkstaden](https://www.vgregion.se/ov/dataverkstad/) at [Västra Götalands Region (VGR)](https://www.vgregion.se/) has been tasked with supporting municipalities and regions with publishing open data. We have created this visualization in order to make it easier to track the progress of each region and their municipalities. 

[MetaSolution](https://entryscape.com/en/) and [Insigo](https://insigo.se/en/) has been tasked by VGR to create a visualization of each regions progress in regards to publication of open data. MetaSolution has contributed with data modelling expertise and file hosting on the Entryscape platform. Insigo has written the code and assisted with technical advice.

## Installation

The graph is created with [D3.js](https://d3js.org/) version 7. The code needed to create the graph is located in main.js. An example of how the graph can be used in an HTML file can be seen in main.html. In the following code the script tag downloads d3 and the meta tag makes the graph responsive. 
```html 
<head>
    <script src="https://d3js.org/d3.v7.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
```
In order to add the actual graph you simply need to include the following code in the body.

```html
<body>
    <svg id="region_status" height="800" width="800" style="width: 100%; height: 100%" viewBox="0 0 800 800"></svg>
    <script src="main.js"></script>
</body>
```
And finally to style the tooltip you can look into style.css. We have tried to add as little css as possible when develping this graph visualization.

## Contribute

Make a pull request in order to contribute to the project.

## License

This project is licensed under the terms of the MIT license.
